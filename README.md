# RedisClientForTermianl

#### 介绍
基于终端可视化交互,纯java代码,使用redisson作为客户端连接redis

#### 软件架构
redisson.  
springboot.  


#### 安装教程
## 下载源码
git clone https://gitee.com/bolingit/redis-client-for-terminal.git   
## 进入目录
cd redis-client-for-terminal
## copy配置文件
cp src/main/resources/application.yml ./
## 编译打包
mvn package
## 修改配置
vim application.yml
## 启动
java -jar target/terminal.jar

#### 操作说明
编译完成后可以复制jar包到 自己的目录，在jar包目录下直接创建application.yml文件添加配置就可以了。基于springboot自动加载顺序，当前目录配置会覆盖jar包中的application.yml



![alt demo](https://gitee.com/bolingit/redis-client-for-terminal/raw/master/demo.png)