package group.sshd.redis.client.terminal;

import com.googlecode.lanterna.terminal.Terminal;
import group.sshd.redis.client.terminal.ui.TerminalUI;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.io.IOException;

/**
 * @author bolin
 * @create 2020-11
 * @desc
 */
@SpringBootApplication
@EnableAutoConfiguration
public class ApplicationRun {

    public static void main(String[] args) throws IOException {
        ApplicationContext context = SpringApplication.run(ApplicationRun.class, args);
        TerminalUI ui = context.getBean(TerminalUI.class);
        ui.run();
        Terminal terminal = context.getBean(Terminal.class);
        terminal.putCharacter(' ');
        terminal.close();
        System.exit(0);
    }


}
