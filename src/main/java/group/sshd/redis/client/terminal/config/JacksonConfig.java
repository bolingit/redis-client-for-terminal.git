package group.sshd.redis.client.terminal.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author bolin
 * @create 2020-11
 * @desc 配置jackson
 */
@Configuration
public class JacksonConfig {

    @Bean
    public ObjectMapper objectMapper(){
        return new ObjectMapper();
    }


}
