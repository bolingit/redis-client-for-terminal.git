package group.sshd.redis.client.terminal.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Map;

/**
 * @author bolin
 * @create 2020-11
 * @desc 配置redisson
 */
@Configuration
@ConfigurationProperties(prefix = "redisson.config")
public class RedisConfig {

    public Map<String, Map<String, String>> nodeMap;

    public Map<String, Map<String, String>> getNodeMap() {
        return nodeMap;
    }

    public void setNodeMap(Map<String, Map<String, String>> nodeMap) {
        this.nodeMap = nodeMap;
    }

    public String[] getNodeNames(){
        return nodeMap.keySet().toArray(new String[nodeMap.size()]);
    }


}
