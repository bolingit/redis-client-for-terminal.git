package group.sshd.redis.client.terminal.config;

import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.SimpleTheme;
import com.googlecode.lanterna.gui2.MultiWindowTextGUI;
import com.googlecode.lanterna.gui2.SeparateTextGUIThread;
import com.googlecode.lanterna.gui2.WindowBasedTextGUI;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.screen.TerminalScreen;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

/**
 * @author bolin
 * @create 2020-11
 * @desc
 */
@Configuration
public class TerminalConfig {

    @Autowired
    private Terminal terminal;

    @Bean
    public Terminal terminal() throws IOException {
        Terminal terminal = new DefaultTerminalFactory().createTerminal();
        terminal.putCharacter(' ');// 空白填充
        return terminal;
    }


    @Bean
    public Screen screen(Terminal terminal) throws IOException {
        Screen screen = new TerminalScreen(terminal);
        screen.startScreen(); // 开始渲染
        return screen;
    }

    @Bean
    public WindowBasedTextGUI gui(Screen screen) {
        MultiWindowTextGUI gui = new MultiWindowTextGUI(new SeparateTextGUIThread.Factory(), screen);
        gui.setTheme(SimpleTheme.makeTheme(false, TextColor.ANSI.DEFAULT, TextColor.ANSI.DEFAULT, TextColor.ANSI.DEFAULT, TextColor.ANSI.DEFAULT, TextColor.ANSI.BLACK/*字体变黑*/, TextColor.ANSI.WHITE/*背景变白*/, TextColor.ANSI.DEFAULT));
        return gui;
    }

    @Bean
    public SeparateTextGUIThread guiThread(WindowBasedTextGUI gui) {
        SeparateTextGUIThread guiThread = (SeparateTextGUIThread) gui.getGUIThread();
        guiThread.start();
        return guiThread;
    }

}
